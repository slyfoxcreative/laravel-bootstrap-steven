@if ($errors->any())
  <x-bs-alert style="danger">
    <p>
      There was a problem with your input.
    </p>
    <ul class="mb-0">
      @foreach ($errors->all() as $message)
        <li>{{ $message }}</li>
      @endforeach
    </ul>
  </x-bs-alert>
@endif
@foreach (['status', 'success', 'info', 'warning', 'danger'] as $key)
  @if (session()->has($key))
    <x-bs-alert :style="($key === 'status') ? 'success' : $key">
      {{ session($key) }}
    </x-bs-alert>
  @endif
@endforeach
