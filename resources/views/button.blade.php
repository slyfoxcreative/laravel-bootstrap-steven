<{{ $tag }} {{ $attributes->merge($additionalAttributes()) }}>
  {{ $slot }}
</{{ $tag }}>
