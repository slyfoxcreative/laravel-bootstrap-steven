# Laravel Bootstrap

Helpers and Blade components for the Bootstrap framework in Laravel.

## Installation

`composer require slyfoxcreative/laravel-bootstrap`

## Getting Started

If you're using Laravel 5.4, add `SlyFoxCreative\Bootstrap\ServiceProvider` to
the `providers` array in `config/app.php`.
