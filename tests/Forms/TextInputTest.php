<?php

declare(strict_types=1);

namespace SlyFoxCreative\Bootstrap\Tests\Forms;

use Illuminate\Support\MessageBag;
use SlyFoxCreative\Bootstrap\Tests\TestCase;

use function SlyFoxCreative\Html\text;

class TextInputTest extends TestCase
{
    public function testTextInput(): void
    {
        self::assertEquals(
            "<input class='form-control' id='test' name='test' type='text'>",
            text('test'),
        );
    }

    public function testTextInputWithSize(): void
    {
        self::assertEquals(
            "<input class='form-control form-control-lg' id='test' name='test' type='text'>",
            text('test', ['size' => 'lg']),
        );
    }

    public function testTextInputWithSessionError(): void
    {
        $bag = new MessageBag();
        $bag->add('test', 'Error message');
        session(['errors' => $bag]);

        self::assertEquals(
            "<input class='form-control is-invalid' id='test' name='test' type='text'>",
            text('test'),
        );
    }

    public function testTextInputWithNestedSessionError(): void
    {
        $bag = new MessageBag();
        $bag->add('test.nested', 'Error message');
        session(['errors' => $bag]);

        self::assertEquals(
            "<input class='form-control is-invalid' id='test_nested' name='test[nested]' type='text'>",
            text('test[nested]'),
        );
    }

    public function testTextInputWithSessionErrorOverriddenByErrorArgument(): void
    {
        $bag = new MessageBag();
        $bag->add('test', 'Error message');
        session(['errors' => $bag]);

        self::assertEquals(
            "<input class='form-control' id='test' name='test' type='text'>",
            text('test', ['error' => false]),
        );
    }
}
