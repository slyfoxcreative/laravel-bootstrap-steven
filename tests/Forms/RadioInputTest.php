<?php

declare(strict_types=1);

namespace SlyFoxCreative\Bootstrap\Tests\Forms;

use SlyFoxCreative\Bootstrap\Tests\TestCase;

use function SlyFoxCreative\Html\radio;

class RadioInputTest extends TestCase
{
    public function testRadioInput(): void
    {
        self::assertEquals(
            "<input class='form-check-input' id='test_1' name='test' type='radio' value='1'>",
            radio('test', ['value' => 1]),
        );
    }

    public function testClass(): void
    {
        self::assertEquals(
            "<input class='form-check-input test' id='test' name='test' type='radio'>",
            radio('test', ['class' => ['test']]),
        );
    }

    public function testDisabled(): void
    {
        self::assertEquals(
            "<input class='form-check-input' disabled id='test_1' name='test' type='radio' value='1'>",
            radio('test', ['disabled' => true, 'value' => 1]),
        );
    }
}
