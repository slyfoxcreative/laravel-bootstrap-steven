<?php

declare(strict_types=1);

namespace SlyFoxCreative\Bootstrap\Tests\Forms;

use SlyFoxCreative\Bootstrap\Tests\TestCase;

use function SlyFoxCreative\Html\time;

class TimeInputTest extends TestCase
{
    public function testTimeInput(): void
    {
        self::assertEquals(
            "<input class='form-control' id='test' name='test' type='time'>",
            time('test'),
        );
    }
}
