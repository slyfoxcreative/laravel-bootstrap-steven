<?php

declare(strict_types=1);

namespace SlyFoxCreative\Bootstrap\Tests\Forms;

use SlyFoxCreative\Bootstrap\Tests\TestCase;

use function SlyFoxCreative\Html\label;

class ControlLabelTest extends TestCase
{
    public function testControlLabelDefaultType(): void
    {
        self::assertEquals(
            "<label class='form-label' for='test'>Test</label>",
            label('test', 'Test'),
        );
    }

    public function testControlLabel(): void
    {
        self::assertEquals(
            "<label class='form-check-label' for='test'>Test</label>",
            label('test', 'Test', ['type' => 'check']),
        );
    }

    public function testControlLabelWithNoText(): void
    {
        self::assertEquals(
            "<label class='form-check-label' for='test'>Test</label>",
            label('test', attributes: ['type' => 'check']),
        );
    }
}
