<?php

declare(strict_types=1);

namespace SlyFoxCreative\Bootstrap\Tests\Forms;

use Illuminate\Support\HtmlString;
use SlyFoxCreative\Bootstrap\Tests\TestCase;

use function SlyFoxCreative\Html\select;

class SelectTest extends TestCase
{
    public function testSelect(): void
    {
        $expected = <<<'EOS'
            <select class='form-select test-class' id='test' name='test'>
            <option value='a'>value 1</option>
            <option value='b'>value 2</option>
            </select>
            EOS;
        $expected = new HtmlString($expected);

        self::assertEquals(
            $expected,
            select(
                'test',
                ['a' => 'value 1', 'b' => 'value 2'],
                ['class' => ['test-class']],
            ),
        );
    }

    public function testSelectWithSelectedValue(): void
    {
        $expected = <<<'EOS'
            <select class='form-select' id='test' name='test'>
            <option selected value='1'>value 1</option>
            <option value='2'>value 2</option>
            </select>
            EOS;
        $expected = new HtmlString($expected);

        self::assertEquals(
            $expected,
            select(
                'test',
                [1 => 'value 1', 2 => 'value 2'],
                ['selected' => 1],
            ),
        );
    }

    public function testSelectWithSize(): void
    {
        $expected = <<<'EOS'
            <select class='form-select form-select-lg' id='test' name='test'>
            </select>
            EOS;
        $expected = new HtmlString($expected);

        self::assertEquals($expected, select('test', [], ['size' => 'lg']));
    }
}
