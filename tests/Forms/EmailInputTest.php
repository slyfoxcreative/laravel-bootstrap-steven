<?php

declare(strict_types=1);

namespace SlyFoxCreative\Bootstrap\Tests\Forms;

use SlyFoxCreative\Bootstrap\Tests\TestCase;

use function SlyFoxCreative\Html\email;

class EmailInputTest extends TestCase
{
    public function testEmailInput(): void
    {
        self::assertEquals(
            "<input class='form-control' id='test' name='test' type='email'>",
            email('test'),
        );
    }
}
