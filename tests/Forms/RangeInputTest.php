<?php

declare(strict_types=1);

namespace SlyFoxCreative\Bootstrap\Tests\Forms;

use SlyFoxCreative\Bootstrap\Tests\TestCase;

use function SlyFoxCreative\Html\range;

class RangeInputTest extends TestCase
{
    public function testRangeInput(): void
    {
        self::assertEquals(
            "<input class='form-range' id='test' name='test' type='range'>",
            range('test'),
        );
    }
}
