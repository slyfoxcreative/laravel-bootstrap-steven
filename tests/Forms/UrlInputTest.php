<?php

declare(strict_types=1);

namespace SlyFoxCreative\Bootstrap\Tests\Forms;

use SlyFoxCreative\Bootstrap\Tests\TestCase;

use function SlyFoxCreative\Html\url;

class UrlInputTest extends TestCase
{
    public function testUrlInput(): void
    {
        self::assertEquals(
            "<input class='form-control' id='test' name='test' type='url'>",
            url('test'),
        );
    }
}
