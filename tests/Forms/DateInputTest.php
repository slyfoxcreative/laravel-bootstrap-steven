<?php

declare(strict_types=1);

namespace SlyFoxCreative\Bootstrap\Tests\Forms;

use SlyFoxCreative\Bootstrap\Tests\TestCase;

use function SlyFoxCreative\Html\date;

class DateInputTest extends TestCase
{
    public function testDateInput(): void
    {
        self::assertEquals(
            "<input class='form-control' id='test' name='test' type='date'>",
            date('test'),
        );
    }
}
