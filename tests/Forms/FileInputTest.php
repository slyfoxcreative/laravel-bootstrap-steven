<?php

declare(strict_types=1);

namespace SlyFoxCreative\Bootstrap\Tests\Forms;

use SlyFoxCreative\Bootstrap\Tests\TestCase;

use function SlyFoxCreative\Html\file;

class FileInputTest extends TestCase
{
    public function testFileInput(): void
    {
        self::assertEquals(
            "<input class='form-control' id='test' name='test' type='file'>",
            file('test'),
        );
    }

    public function testClass(): void
    {
        self::assertEquals(
            "<input class='form-control test' id='test' name='test' type='file'>",
            file('test', ['class' => ['test']]),
        );
    }
}
