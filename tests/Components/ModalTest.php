<?php

declare(strict_types=1);

namespace SlyFoxCreative\Bootstrap\Tests\Components;

use SlyFoxCreative\Bootstrap\Components\Modal;
use SlyFoxCreative\Bootstrap\Tests\TestCase;

class ModalTest extends TestCase
{
    public function testDefaultAttributeValues(): void
    {
        $modal = new Modal('modal-id');

        $expected = [
            'id' => 'modal-id',
            'tabindex' => '-1',
            'aria-labelledby' => 'modal-id-label',
            'aria-hidden' => 'true',
        ];

        self::assertSame($expected, $modal->additionalAttributes());
    }

    public function testStaticAttribute(): void
    {
        $modal = new Modal('modal-id', static: true);

        $expected = [
            'id' => 'modal-id',
            'tabindex' => '-1',
            'aria-labelledby' => 'modal-id-label',
            'aria-hidden' => 'true',
            'data-bs-backdrop' => 'static',
            'data-bs-keyboard' => 'false',
        ];

        self::assertSame($expected, $modal->additionalAttributes());
    }

    public function testRender(): void
    {
        $modal = new Modal('modal-id');

        self::assertSame('bootstrap::modal', $modal->render()->name());
    }
}
