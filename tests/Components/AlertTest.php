<?php

declare(strict_types=1);

namespace SlyFoxCreative\Bootstrap\Tests\Components;

use SlyFoxCreative\Bootstrap\Components\Alert;
use SlyFoxCreative\Bootstrap\Tests\TestCase;

class AlertTest extends TestCase
{
    public function testDefaultAttributeValues(): void
    {
        $alert = new Alert();

        self::assertFalse($alert->dismissible);
        self::assertSame(
            ['role' => 'alert', 'class' => 'alert alert-primary'],
            $alert->additionalAttributes(),
        );
    }

    public function testAttributes(): void
    {
        $alert = new Alert('secondary', true);

        self::assertTrue($alert->dismissible);
        self::assertSame(
            ['role' => 'alert', 'class' => 'alert alert-secondary alert-dismissible fade show'],
            $alert->additionalAttributes(),
        );
    }

    public function testRender(): void
    {
        $alert = new Alert();

        self::assertSame('bootstrap::alert', $alert->render()->name());
    }
}
