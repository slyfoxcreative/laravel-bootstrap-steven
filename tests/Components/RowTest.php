<?php

declare(strict_types=1);

namespace SlyFoxCreative\Bootstrap\Tests\Components;

use SlyFoxCreative\Bootstrap\Components\Row;
use SlyFoxCreative\Bootstrap\Tests\TestCase;

class RowTest extends TestCase
{
    public function testDefaultAttributeValues(): void
    {
        $row = new Row();

        self::assertSame(['class' => 'row'], $row->additionalAttributes());
        self::assertSame('div', $row->tag);
    }

    public function testAttributes(): void
    {
        $row = new Row('fieldset', true);

        self::assertSame(['class' => 'row justify-content-center'], $row->additionalAttributes());
        self::assertSame('fieldset', $row->tag);
    }

    public function testRender(): void
    {
        $row = new Row();

        self::assertSame('bootstrap::row', $row->render()->name());
    }
}
