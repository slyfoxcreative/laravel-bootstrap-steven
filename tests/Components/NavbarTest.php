<?php

declare(strict_types=1);

namespace SlyFoxCreative\Bootstrap\Tests\Components;

use SlyFoxCreative\Bootstrap\Components\Navbar;
use SlyFoxCreative\Bootstrap\Tests\TestCase;

class NavbarTest extends TestCase
{
    public function testAttributes(): void
    {
        $navbar = new Navbar('sm', 'light', 'primary', false);

        self::assertSame('sm', $navbar->expandSize);
        self::assertSame('light', $navbar->style);
        self::assertSame('primary', $navbar->color);
        self::assertFalse($navbar->collapsible);
    }

    public function testDefaultAttributes(): void
    {
        $navbar = new Navbar('sm', 'light', 'primary');

        self::assertTrue($navbar->collapsible);
    }

    public function testRender(): void
    {
        $navbar = new Navbar('sm', 'light', 'primary');

        self::assertSame('bootstrap::navbar', $navbar->render()->name());
    }
}
