<?php

declare(strict_types=1);

namespace SlyFoxCreative\Bootstrap\Components;

use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

/**
 * Bootstrap modal component.
 *
 * Special attributes:
 *  - id: the modal's HTML id
 *  - static: whether to to give the modal a static backdrop, preventing it from
 *            being dismissed by clicking outside it or hitting the escape key
 */
class Modal extends Component
{
    public function __construct(
        public string $id,
        private bool $static = false,
    ) {}

    /** @return non-empty-array<string, string> */
    public function additionalAttributes(): array
    {
        $attributes = [
            'id' => $this->id,
            'tabindex' => '-1',
            'aria-labelledby' => "{$this->id}-label",
            'aria-hidden' => 'true',
        ];

        if ($this->static) {
            $attributes['data-bs-backdrop'] = 'static';
            $attributes['data-bs-keyboard'] = 'false';
        }

        return $attributes;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View
    {
        return view('bootstrap::modal');
    }
}
