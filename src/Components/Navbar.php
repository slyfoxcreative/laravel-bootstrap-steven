<?php

declare(strict_types=1);

namespace SlyFoxCreative\Bootstrap\Components;

use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

/**
 * Bootstrap navbar component.
 *
 * Special attributes:
 *  - collapsible: whether to make the contents of the navbar collapsible
 *    into a hamburger menu on small screens
 *  - expand-size: screen-size breakpoint at which to expand the navbar if
 *    it's collapsible
 *  - style: light or dark navbar style
 *  - color: background color
 */
class Navbar extends Component
{
    public string $expandSize;

    public string $style;

    public string $color;

    public bool $collapsible;

    public function __construct(string $expandSize, string $style, string $color, bool $collapsible = true)
    {
        $this->expandSize = $expandSize;
        $this->style = $style;
        $this->color = $color;
        $this->collapsible = $collapsible;
    }

    public function render(): View
    {
        return view('bootstrap::navbar');
    }
}
