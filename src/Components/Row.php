<?php

declare(strict_types=1);

namespace SlyFoxCreative\Bootstrap\Components;

use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

/**
 * Bootstrap row component.
 *
 * Special attributes:
 *  - tag: the HTML tag to use for the row
 *  - center: whether to horizontally center the row's contents
 */
class Row extends Component
{
    public string $tag;

    private bool $center;

    public function __construct(string $tag = 'div', bool $center = false)
    {
        $this->tag = $tag;
        $this->center = $center;
    }

    /**
     * Get additional attributes to be merged into the user-supplied
     * attributes.
     *
     * @return array<string, string>
     */
    public function additionalAttributes(): array
    {
        $attributes = [
            'class' => 'row',
        ];

        if ($this->center) {
            $attributes['class'] .= ' justify-content-center';
        }

        return $attributes;
    }

    public function render(): View
    {
        return view('bootstrap::row');
    }
}
